var express = require('express');
var router = express.Router();

router.get('/', function(req, res) {
	var response = require('../response/sample.json');
	res.send(JSON.stringify(response))
});

module.exports = router;